BASE_PATH="/home/likeno/spatial-data-server/data-collections"

poetry run django-admin geospaca-load-data ${BASE_PATH}/arome-products.yml &&
poetry run django-admin geospaca-load-data ${BASE_PATH}/ecmwf-products.yml &&
poetry run django-admin geospaca-load-data ${BASE_PATH}/ipma-products.yml &&
poetry run django-admin geospaca-load-data ${BASE_PATH}/lsasaf-products.yml
