# spatial-data-server

A spatial data server to serve innometeo products via OGC standards


## How to load data into the server

The `data-collections` directory contains a number of YAML files that may
be fed to geospaca cli. These files will generate an initial structure for
the various innometeo products.

Example usage:

```
geospaca-admin load-data spatial-data-server/data-collections/nwp-arome/nwp-arome.yml
```
