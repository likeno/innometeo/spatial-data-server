FROM registry.gitlab.com/likeno/geospaca:0.7.1

USER root
WORKDIR /home/likeno/spatial-data-server
COPY --chown=likeno . .
# RUN chown --recursive likeno:likeno .

WORKDIR /home/likeno/geospaca
USER likeno

ENV \
    DJANGO_MAPSERVER_FONTSET_PATH=/home/likeno/spatial-data-server/fonts/mapserver_fontset \
    DJANGO_MAPSERVER_SYMBOLS_DIR=/home/likeno/spatial-data-server/symbols

